import Cell from './Cell';

/* tslint:disable:no-unused-variable */

export default class Buffer {
    private mWidth = 0;
    private mHeight = 0;
    private mCells: Cell[] = [];

    get width() {
        return this.mWidth;
    }

    get height() {
        return this.mHeight;
    }

    getCell(x: number, y: number): Cell {
        return this.mCells[x + y * this.mWidth];
    }

    resize(width: number, height: number): void {
        this.mWidth = width;
        this.mHeight = height;
        this.mCells = new Array(width*height);
    }
}
