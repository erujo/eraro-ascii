export default class Box {
    private _x = 0;
    private _y = 0;
    private _w = 0;
    private _h = 0;

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }

    get w() {
        return this._w;
    }

    get h() {
        return this._h;
    }

    constructor(x: number, y: number, w: number, h: number) {
        this._x = x;
        this._y = y;
        this._w = w;
        this._h = h;
    }

    isIn(x: number, y: number): boolean {
        if(this._x > x) return false;
        if(this._y > y) return false;
        if(this._x + this._w < x) return false;
        if(this._y + this._h < y) return false;
        return true;
    }
}
