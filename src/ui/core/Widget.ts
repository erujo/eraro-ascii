import ICellChangeListener from './ICellChangeListener'
import Box from  './Box'
import Cell from  './Cell'

/* tslint:disable:no-unused-variable */

export default class Widget{
    private mBox: Box = new Box(0, 0, 0, 0);
    private mListener: ICellChangeListener | null = null;

    get box(): Box {
        return this.mBox;
    }

    set box(box: Box) {
        this.mBox = box;
    }

    get parent(): ICellChangeListener | null {
        return this.mListener;
    }

    set parent(parent: ICellChangeListener | null) {
        this.mListener = parent;
    }

    constructor() {
    }

    getCell(x: number, y: number): Cell | null {
        return null;
    }

}
