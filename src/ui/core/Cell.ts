export default class Cell {
    private _char = '.';

    get char() {
        return this._char;
    }

    constructor(char: string = '-') {
        this._char = char;
    }
}
