import Widget from './Widget'
import Box from './Box'

import ICellChangedListener from './ICellChangeListener'
const term = require( 'terminal-kit' ).terminal ;

export default class Renderer implements ICellChangedListener {
    private mRoot: Widget;
    private mSize = new Box(0, 0, 0, 0);

    set size(box: Box) {
        this.mSize = box;
        // redraw?
    }

    constructor(root: Widget) {
        this.mRoot = root;
        root.parent = this;


    }

    onCellChanged(widget: Widget, x: number, y:number, w:number, h:number): void {
        for(let j = y; j < h + y; j++) {
            term.moveTo(1 + this.mSize.x + x, 1 + this.mSize.y + j);
            for(let i = x; i < w + x + 1; i++) {
                const cell = this.mRoot.getCell(this.mSize.x + i, this.mSize.y + j);
                if(cell == null) {
                    term(' ');
                } else {
                    term(cell.char);
                }
            }
        }
    }

    drawAll(): void {
        for(let j = 0;j<this.mSize.h;j++) {
            process.stdout.write('\n' + this.mSize.h + ":" + this.mSize.w);
        }

        this.onCellChanged(this.mRoot, this.mSize.x, this.mSize.y, this.mSize.w, this.mSize.h)
    }
}
