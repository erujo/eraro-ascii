import Widget from './Widget'

export default interface ICellChangeListener {
    onCellChanged(widget: Widget, x: number, y:number, w:number, h:number): void;
}
