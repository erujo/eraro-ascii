import Renderer from './core/Renderer'
import Container from './widgets/Container';
import Box from './core/Box';
import Frame from './widgets/Frame';
const term = require( 'terminal-kit' ).terminal ;
const size = require('window-size');

export default class UI {
    private mRoot: Container;
    private mRenderer: Renderer;

    constructor() {
        // Create root element
        this.mRoot = new Container();
        // To full screen
        const screen = new Box(0, 0, size.get().width - 1, size.get().height - 1);
        this.mRoot.box = screen;

        process.stdout.on('resize', () => {
            //TODO: resize mRoot
        });

        let frame = new Frame();
        frame.box = new Box(1, 1, 20, 20);
        this.mRoot.addWidget(frame);

        this.mRenderer = new Renderer(this.mRoot);
        this.mRenderer.size = screen;
        this.mRenderer.drawAll();

        this.initKeyboardSupport();
        this.initMouseSupport();
    }

    initKeyboardSupport(): void {
        term.on( 'key' , ( key , matches , data ) => {
            switch ( key )
            {
                case 'CTRL_C' : term.moveTo(size.get().width , size.get().height ); process.exit() ; break ;
                default:
                    // Echo anything else
                    term.noFormat(
                        Buffer.isBuffer( data.code ) ?
                            data.code :
                            String.fromCharCode( data.code )
                    ) ;
                    break ;
            }
        } ) ;
    }

    initMouseSupport(): void {
        term.grabInput( { mouse: 'motion' } ) ;
        term.on( 'mouse' , function( name , data ) {
            //console.log(data);
            term.moveTo( data.x , data.y ) ;
        } ) ;
    }

}
