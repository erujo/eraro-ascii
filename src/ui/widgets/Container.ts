import Widget from '../core/Widget'
import Cell from '../core/Cell'
import ICellChangeListener from '../core/ICellChangeListener'

/* tslint:disable:no-unused-variable */

export default class Container extends Widget implements ICellChangeListener {
    private mWidgets: Widget[] = [];

    addWidget(widget: Widget): void {
        this.mWidgets.push(widget);
        widget.parent = this;
    }

    removeWidget(widget: Widget): void {
        //TODO:
    }

    getCell(x: number, y: number): Cell | null {
        for(let w of this.mWidgets) {
            if(w.box.isIn(x, y))
                return w.getCell(x - w.box.x, y - w.box.y);
        }
        return new Cell('.');
    }

    onCellChanged(widget: Widget, x: number, y:number, w:number, h:number): void {
        if(this.parent == null) return;
        this.parent.onCellChanged(this, x + widget.box.x, y + widget.box.y, w, h);
    }
}
