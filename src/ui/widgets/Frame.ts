import Widget from '../core/Widget';
import Cell from '../core/Cell'

export default class Frame extends Widget {
    private mTopChar = '-';
    constructor() {
        super();
        setInterval( () => {
            this.changeFrame();
        }, 1000);
    }

    getCell(x: number, y: number): Cell | null {
        if(x == 0 || y == 0 ||
             x == this.box.w || y == this.box.h) {
            return new Cell(this.mTopChar);
        }
        return null;
    }

    changeFrame() {
        // Change char
        if(this.mTopChar == '-') this.mTopChar = '=';
        else this.mTopChar = '-';

        // Redraw frame
        if(this.parent == null) return;
        this.parent.onCellChanged(this, 0, 0, this.box.w, 1);
    }
}
